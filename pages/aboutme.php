<div class="section" id="about">
  <div class="container">
    <div class="card" data-aos="fade-up" data-aos-offset="10">
      <div class="row">
        <div class="col-lg-6 col-md-12">
          <div class="card-body">
            <div class="h4 mt-0 title">Deskripsi</div>
            <p>Halo! Perkenalkan saya A Alkhaer Abdillah. Kalian bisa memanggil saya AL, lahir di Ujung Pandang sekarang Makassar, 7 Desember 1994 </p>
            <p>Saya berasal dari Kabupaten Pinrang Sulawesi-Selatan, merupakan lulusan STMIK AKAKOM Yogyakarta tahun 2020 jurusan Teknik Informatika, </p>
            <p>Saat ini saya befokus pada bidang web developer dengan keahlian utama yaitu PHP Laravel, namun tidak menutup kemungkinan untuk belajar hal-hal baru seperti Android, Desktop dll.
                </p>
                <!-- <a href="https://templateflip.com/templates/creative-cv/" target="_blank">Learn More</a> -->
          </div>
        </div>
        <div class="col-lg-6 col-md-12">
          <div class="card-body">
            <div class="h4 mt-0 title">Tentang Saya</div>
            <div class="row">
              <div class="col-sm-4"><strong class="text-uppercase">Umur:</strong></div>
              <div class="col-sm-8">
                <?php
                
                $myBirthday = new DateTime('1994-12-07');
                $today = new DateTime('today');
                echo $today->diff($myBirthday)->y;
              
              
                ?>
              </div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Email:</strong></div>
              <div class="col-sm-8">aalkhaer07@gmail.com</div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Whatsapp:</strong></div>
              <div class="col-sm-8">0851-5958-8005</div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Domisili:</strong></div>
              <div class="col-sm-8">Nologaten gg. Temu Giring no. 27 A, Kel. Caturtunggal, Kec. Depok, Sleman, DIY</div>
            </div>
            <div class="row mt-3">
              <div class="col-sm-4"><strong class="text-uppercase">Minat:</strong></div>
              <div class="col-sm-8">Web Developer</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>