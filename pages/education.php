<div class="section">
  <div class="container cc-education">
    <div class="h4 text-center mb-4 title">Education</div>
    <div class="card">
      <div class="row">
        <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body cc-education-header">
            <p>2013 - 2020</p>
            <div class="h5">Sarjana Strata 1</div>
          </div>
        </div>
        <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body">
            <div class="h5">STMIK AKAKOM Yogyakarta</div>
            <p class="category">Teknik Informatika</p>
            <p>Dengan Judul Tugas Akhir :<br/>Analisa Perbandingan Metode Naive Bayes dan Certainty Factor Dalam Mnediagnosa Hama dan Penyakit Tanaman Kopi</p>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="row">
        <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body cc-education-header">
            <p>2010 - 2013</p>
            <div class="h5">Sekolah Menengah Atas</div>
          </div>
        </div>
        <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body">
            <div class="h5">SMA Negeri 1 Pinrang</div>
            <p class="category">Ilmu Pengetahuan Alam</p>
            <!-- <p>Euismod massa scelerisque suspendisse fermentum habitant vitae ullamcorper magna quam iaculis, tristique sapien taciti mollis interdum sagittis libero nunc inceptos tellus, hendrerit vel eleifend primis lectus quisque cubilia sed mauris. Lacinia porta vestibulum diam integer quisque eros pulvinar curae, curabitur feugiat arcu vivamus parturient aliquet laoreet at, eu etiam pretium molestie ultricies sollicitudin dui.</p> -->
          </div>
        </div>
      </div>
    </div>
  </div>
</div>