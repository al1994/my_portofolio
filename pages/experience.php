<div class="section" id="experience">
  <div class="container cc-experience">
    <div class="h4 text-center mb-4 title">Pengalaman Kerja</div>
    <div class="card">
      <div class="row">
        <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body cc-experience-header">
            <p>September 2020 - Mei 2021</p>
            <div class="h5">Staff IT</div>
            <p>Full Time</p>
          </div>
        </div>
        <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body">
            <div class="h5">Sasuka Online Indonesia</div>
            <!-- <p class="category">Pengembang Web</p> -->
            <p>Sebuah perusahaan penyedia berbagai layanan seperti aplikasi Sasuka Online, Retail Ritzuka, Toko Sekitar dll. Apliksi Sasuka Online adalah sebuah aplikasi multifungsi yang diciptakan khusus untuk belanja dan bisnis online (sumber: <a href="https://sasuka.co.id/">https://sasuka.co.id/</a>)
            </p>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="row">
        <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body cc-experience-header">
            <p>2018</p>
            <div class="h5">Pewawancara</div>
            <p>Part Time</p>
          </div>
        </div>
        <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body">
            <div class="h5">Research Center Media Group</div>
            <!-- <p class="category">Pewawancara</p> -->
            <p>RCMG merupakan sebuah lembaga riset berbasis media di bawah naungan media group yang melakukan survei tentang “Isu –Isu Sosial Kemasyarakatan di Provinsi DIY ,khususnya di Kabupaten Gunungkidul, Kabupaten Bantul dan Kabupaten Sleman
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- <div class="card">
      <div class="row">
        <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body cc-experience-header">
            <p>April 2014 - March 2016</p>
            <div class="h5">WebNote</div>
          </div>
        </div>
        <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body">
            <div class="h5">Web Developer</div>
            <p>Euismod massa scelerisque suspendisse fermentum habitant vitae ullamcorper magna quam iaculis, tristique sapien taciti mollis interdum sagittis libero nunc inceptos tellus, hendrerit vel eleifend primis lectus quisque cubilia sed mauris. Lacinia porta vestibulum diam integer quisque eros pulvinar curae, curabitur feugiat arcu vivamus parturient aliquet laoreet at, eu etiam pretium molestie ultricies sollicitudin dui.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="row">
        <div class="col-md-3 bg-primary" data-aos="fade-right" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body cc-experience-header">
            <p>April 2013 - February 2014</p>
            <div class="h5">WEBM</div>
          </div>
        </div>
        <div class="col-md-9" data-aos="fade-left" data-aos-offset="50" data-aos-duration="500">
          <div class="card-body">
            <div class="h5">Intern</div>
            <p>Euismod massa scelerisque suspendisse fermentum habitant vitae ullamcorper magna quam iaculis, tristique sapien taciti mollis interdum sagittis libero nunc inceptos tellus, hendrerit vel eleifend primis lectus quisque cubilia sed mauris. Lacinia porta vestibulum diam integer quisque eros pulvinar curae, curabitur feugiat arcu vivamus parturient aliquet laoreet at, eu etiam pretium molestie ultricies sollicitudin dui.</p>
          </div>
        </div>
      </div>
    </div> -->
  </div>
</div>